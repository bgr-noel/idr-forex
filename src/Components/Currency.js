import React, { Component } from 'react';

export default class Currency extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.name}</td>
                <td>{(this.props.value * 1.02).toFixed(2)}</td>
                <td>{this.props.value.toFixed(2)}</td>
                <td>{(this.props.value * 0.98).toFixed(2)}</td>
            </tr>
        );
    }
}
