import React, { Component } from 'react';
// import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import Currency from './Components/Currency';

export default class App extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      value: 100000,
      currency: "IDR",
      rates: {}
    };

    this.handleChangeValue = this.handleChangeValue.bind(this);
    this.handleChangeCurrency = this.handleChangeCurrency.bind(this);
    this.getExchangeRates = this.getExchangeRates.bind(this);
  }

  handleChangeCurrency(event) {
    this.setState({
      currency: event.target.value,
      value: (event.target.value === "IDR" ? 100000 : 1)
    })
  }

  handleChangeValue(event) {
    this.setState({value: event.target.value.replace(/\D/,'')})
  }

  getExchangeRates() {
    fetch(`https://api.exchangeratesapi.io/latest?base=${this.state.currency}`)
      .then(res => res.json())
      .then(data => {
        this.setState({ rates: data.rates });
      });      
  }

  componentDidUpdate(prevState) {
    if (prevState.currency !== this.state.currency) {
      this.getExchangeRates();
    }
  }  
  
  componentDidMount() {
    this.getExchangeRates();
  }  

  render() {
    return (
      <div className="App-body">
        <div className="forex container">
          <div className="header mb-4">
            <h1>Forex Table</h1>
          </div>
          <div className="idr-input mb-4">
            <div className="form-group row">
              {/* <label htmlFor="idrValue" className="col-sm-2 col-form-label col-form-label-lg">IDR</label> */}
              <select value={this.state.currency} onChange={this.handleChangeCurrency} className="custom-select custom-select-lg col-sm-2">
                <option value="IDR">IDR</option>
                <option value="USD">USD</option>
                <option value="CAD">CAD</option>
              </select>
              <div className="col-sm-10">
                <input placeholder="Currency Value" type="text" className="form-control form-control-lg" id="idrValue" value={this.state.value} onChange={this.handleChangeValue}/>
              </div>
            </div>
          </div>
          <table className="forex-table">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>BUY</th>
                <th>RATE</th>
                <th>SELL</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.currency !== "IDR" &&
                <Currency name="IDR" value={this.state.rates.IDR * this.state.value} />
              }
              {
                this.state.currency !== "USD" &&
                <Currency name="USD" value={this.state.rates.USD * this.state.value} />
              }
              {
                this.state.currency !== "CAD" &&
                <Currency name="CAD" value={this.state.rates.CAD * this.state.value} />
              }
              <Currency name="JPY" value={this.state.rates.JPY * this.state.value} />
              <Currency name="CHF" value={this.state.rates.CHF * this.state.value} />
              <Currency name="EUR" value={this.state.rates.EUR * this.state.value} />              
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
